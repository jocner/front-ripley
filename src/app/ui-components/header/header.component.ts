import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
   private router: Router
  ) { }

  home() {
    
    this.router.navigate(['/home']);
    
  }

  registro() {
    
    this.router.navigate(['/registro']);
    
  }

  transferir() {
    this.router.navigate(['/transferencia']);
  }

  cartola() {
    
    this.router.navigate(['/historial']);
  }

  ngOnInit(): void {

    
  }


  

}

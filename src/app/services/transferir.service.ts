import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Historial } from '../models/historial.model';
import { Transferir } from '../interfaces/transferir';
import { Busqueda } from '../interfaces/busqueda';

const base_url = environment.base_url;



@Injectable({
  providedIn: 'root'
})
export class TransferirService {

  constructor( private http: HttpClient, 
    private router: Router,
    private ngZone: NgZone) { }

  // busqueda( formData: Busqueda[]){

  //   let param = this.busqueda
  //   return this.http.post(`${ base_url }/busqueda`, formData)
  //   .pipe(
  //     tap( (resp: any) => {
  //       console.log('desde servicio', JSON.stringify(resp));
  //       //let data = {resp}
  //       sessionStorage.setItem('busqueda', JSON.stringify(resp))
  //     })
  //   )
  // }  

  transferencia(formiInfo: Transferir){

    return this.http.post(`${ base_url }/transferir`, formiInfo )
    .pipe(
      tap( (resp: any) => {
        console.log('desde servicio', JSON.stringify(resp));
        sessionStorage.setItem('data', JSON.stringify(resp))
      })
    )


  }

}

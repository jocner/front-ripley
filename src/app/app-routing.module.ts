import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('../app/components/home/home.module').then( m => m.HomeModule)
  },
  { 
    path: 'registro',
    loadChildren: () => import('../app/components/registro/registro.module').then( m => m.RegistroModule )
  },
  {
    path: 'historial',
    loadChildren: () => import('../app/components/historial/historial.module').then( m => m.HistorialModule)
  },
  {
    path: 'transferencia',
    loadChildren: () => import('../app/components/transferencia/transferencia.module').then( m => m.TransferenciaModule)
  }
  
  
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot( routes ),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

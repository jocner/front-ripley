export class Cliente {

    constructor(
        public nombre: string,
        public rut: string,
        public email: string,
        public telefono: string,
        public bancodestino: string,
        public tipocuenta: string,
        public numerocuenta:string
    ) {}


}



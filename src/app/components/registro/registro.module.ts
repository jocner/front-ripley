import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroRoutingModule } from './registro-routing.module';
import { RegistroComponent } from './registro.component';
import { HeaderModule } from '../../ui-components/header/header.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import { from } from 'rxjs';




@NgModule({
  declarations: [RegistroComponent],
  imports: [
    CommonModule,
    RegistroRoutingModule,
    HeaderModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderModule
    ]
})
export class RegistroModule { }

import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { TransferirService } from '../../services/transferir.service';
import { BusquedaService } from '../../services/busqueda.service';
//import { FilterPipe } from '../../filter.pipe/';
import { Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.css']
})
export class TransferenciaComponent implements OnInit {

  public formSubmitted = false;
  filterPost = '';
  
  transferirForm1 = [];
  
  
  

  
 
  result = sessionStorage.getItem('cliente');
  post = JSON.parse(this.result);

  result2 = sessionStorage.getItem('buscado');
  buscado = JSON.parse(this.result2);

  



  public busquedaForm  = this.fb.group({
    email: ['', [ Validators.required, Validators.email ] ]
  });
  

  constructor(
    private fb: FormBuilder,
    public transferirServicio: TransferirService,
    public busquedaServicio: BusquedaService,
    public router: Router
  ) { }

  



    

  resul3 = sessionStorage.getItem('for');
  info = JSON.parse(this.resul3) ? JSON.parse(this.resul3) : '';
  


    public transferirForm = this.fb.group({
    monto: ['', [Validators.required]],
    nombre:  [this.info.nombre , [Validators.required]] ,
    banco: [this.info.bancodestino , [Validators.required]],
    rut: [this.info.rut  , [Validators.required]],
    tipocuenta: [this.info.tipocuenta , [Validators.required]]
  
  }); 



  
  
   transferir() {
    this.formSubmitted = true;
    console.log(this.transferirForm.value);

   

    this.transferirServicio.transferencia( this.transferirForm.value ).subscribe(
       res => {

        if ( this.transferirForm.invalid ) {
          return;
        }

        if(res){
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Transferencia exitosa',
            showConfirmButton: false,
            timer: 1300
          })
        }

        // Navegar al Dashboard
        this.router.navigate(['/home']);

        console.log("info2", res)
        

    }, (err) => {
      // Si sucede un error
      Swal.fire('Error', err.error.msg, 'error' );
      console.log(" detalle error",err);
    })



  }

  


  

  

 

  ngOnInit(): void {

    this.busquedaServicio.clientes().subscribe(
      res => {

       
       console.log("cliente desde transferir", res);

       
       console.log("respuesta2", this.post.nombre);

   }, (err) => {
     // Si sucede un error
     console.log(" detalle error",err);
   })
    
    
  
    console.log("filter", this.buscado);

    
    
   

  console.log("natural", this.buscado);
    for (const cliente of this.buscado) {

      sessionStorage.setItem('for', JSON.stringify(cliente));
    
      
      

   
     
      
    }

   

    console.log("funcion", this.info);

    
     

  }

 
 


  

  

}

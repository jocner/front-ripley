import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferenciaRoutingModule } from './transferencia-routing.module';
import { TransferenciaComponent } from './transferencia.component';
import { HeaderModule } from '../../ui-components/header/header.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FilterPipe } from '../transferencia/filter.pipe';
import { from } from 'rxjs';



@NgModule({
  declarations: [TransferenciaComponent,
  FilterPipe
],
  imports: [
    CommonModule,
    TransferenciaRoutingModule,
    HeaderModule,
    FormsModule,
    ReactiveFormsModule,
   // FilterPipe
  ],
  exports: [
    TransferenciaComponent
  ]
})
export class TransferenciaModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistorialComponent } from './historial.component';
import { HistorialRoutingModule } from './historial-routing.module';
import { HeaderModule } from '../../ui-components/header/header.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { from } from 'rxjs';



@NgModule({
  declarations: [HistorialComponent],
  imports: [
    CommonModule,
    HistorialRoutingModule,
    HeaderModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HistorialModule { }

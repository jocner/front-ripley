import { Component, OnInit } from '@angular/core';
import { BancosService } from '../../services/bancos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public bancosServices: BancosService,
  ) { }

  ngOnInit(): void {

    this.bancosServices.bancos().subscribe(
      res => {

        console.log("servicio bancos", res);

      })

  }

}

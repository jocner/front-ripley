// export interface Busqueda {

//   email: string 
// }

export interface Busqueda {

  email: string,
  nombre: string,
  rut: string,
  bancodestino: string,
  tipocuenta: string

}

export interface Registro {

 nombre: string,
 rut: string,
 email: string,
 telefono: string,
 bancodestino: string,
 tipocuenta: string,
 numerocuenta:string

}
